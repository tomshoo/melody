#ifndef __I_COMPOSITOR_HH_
#define __I_COMPOSITOR_HH_

extern "C" {
#include <wayland-server.h>
#include <wlr/backend.h>
#include <wlr/types/wlr_drm.h>
#include <wlr/types/wlr_linux_dmabuf_v1.h>

#define static
#include <render/gles2.h>
#include <wlr/render/wlr_renderer.h>
#undef static
}

namespace melody {
class _state {
    struct wl_display*    display;
    struct wl_event_loop* evloop;

    struct wlr_backend*  backend;
    long int             drm_fd;
    struct wlr_renderer* renderer;
    wlr_linux_dmabuf_v1* linux_dmabuf;

    friend class server;
};

class server {
    _state* state;

public:
    server();

    void init_backend(void);

    ~server();
};
}; // namespace melody

#endif
