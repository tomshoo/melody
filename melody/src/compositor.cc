#include "compositor.hh"

#include <cerrno>
#include <cstdio>
#include <cstdlib>

using namespace melody;

server::server()
    : state(new _state) {}

server::~server() {
    delete this->state;
}

static void err_and_die(char const err[]) {
    std::perror(err);
    std::exit(errno);
}

void server::init_backend() {
    auto& state = *this->state;

    if (not(state.display = wl_display_create())) {
        err_and_die("wl_display_create");
    }

    if (not(state.evloop = wl_display_get_event_loop(state.display))) {
        err_and_die("wl_display_get_event_loop");
    }

    if (not(state.backend = wlr_backend_autocreate(state.display, nullptr))) {
        err_and_die("wlr_backend_autocreate");
    }

    if ((state.drm_fd = wlr_backend_get_drm_fd(state.backend)) < 0) {
        err_and_die("wlr_backend_get_drm_fd");
    }

    if (not(state.renderer = wlr_gles2_renderer_create_with_drm_fd(state.drm_fd))) {
        err_and_die("wlr_gles2_renderer_create_with_drm_fd");
    }

    wlr_renderer_init_wl_shm(state.renderer, state.display);

    if (auto _texture_formats = wlr_renderer_get_dmabuf_texture_formats(state.renderer)) {
        if (wlr_renderer_get_drm_fd(state.renderer) >= 0)
            wlr_drm_create(state.display, state.renderer);

        state.linux_dmabuf = wlr_linux_dmabuf_v1_create_with_renderer(state.display, 4, state.renderer);
    }
}
