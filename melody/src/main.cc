#include "compositor.hh"

int main(int const argc, char* const argv[]) {
    melody::server server{};
    server.init_backend();
    return 0;
}
